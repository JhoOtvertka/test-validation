﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;
using TestValidation.Models;

namespace TestValidation.Controllers
{
    public class TestController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserValidator _validator;

        public TestController(ILogger<HomeController> logger, IValidator validator)
        {
            _logger = logger;
            _validator = validator as UserValidator;
        }
        

        [HttpGet]
        public IActionResult LoginFromQuery()
        {
            return View("LoginUser");
        }

        [HttpPost]
        public ActionResult LoginFromQuery(string login, string password)
        {
            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                return Content("Ошибка валидации");
            }
            return Content($"Добрый вечер, {login} : {password}");
        }


        [HttpGet]
        public IActionResult LoginFromUser()
        {
            return View("LoginUser");
        }

        [HttpPost]
        public ActionResult LoginFromUser(User user)
        {
            if (string.IsNullOrEmpty(user?.Login) || string.IsNullOrEmpty(user.Password))
            {
                return Content("Ошибка валидации");
            }
            return Content($"Добрый вечер, {user.Login} : {user.Password}");
        }


        [HttpGet]
        public IActionResult LoginFromBody()
        {
            return View("LoginUser");
        }

        [HttpPost]
        public ActionResult LoginFromBody(User user)
        {
            if (!ModelState.IsValid)
            {
                return Content("Ошибка валидации");
            }

            return Content($"Добрый вечер, {user.Login} : {user.Password}");
        }


        [HttpGet]
        public bool VerifyPhone(string Phone)
        {
            Regex regex = new Regex(@"^\+79\d{9}$");
            return regex.IsMatch(Phone);
        }

        [HttpGet]
        public IActionResult LoginUserNext()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginUserNext(UserNext user)
        {
            if (!ModelState.IsValid)
            {
                return Content("Ошибка валидации");
            }

            return Content($"Добрый вечер, {user.Login} : {user.Passport}");
        }


        [HttpGet]
        public IActionResult LoginFromValidator()
        {
            return View("LoginUser");
        }

        [HttpPost]
        public ActionResult LoginFromValidator(User user)
        {
            if (!_validator.Validate(user).IsValid)
            {
                return Content("Ошибка валидации из валидатора");
            }

            return Content($"Добрый вечер, {user.Login} : {user.Password}");
        }
    }
}
