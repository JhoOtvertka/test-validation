﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace TestValidation.Models
{
    public class RussianPassportAttribute : ValidationAttribute
    {
        public RussianPassportAttribute()
        { }

        public string GetErrorMessage() => "Не является русским паспортом";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Regex regex = new Regex(@"\d{4}\s\d{6}");
            var result = regex.IsMatch(value.ToString());
            if (!result)
            {
                return new ValidationResult(GetErrorMessage());
            }
            return ValidationResult.Success;
        }
    }
}
