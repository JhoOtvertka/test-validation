﻿using System.ComponentModel.DataAnnotations;

namespace TestValidation.Models
{
    public class User
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [MinLength(8, ErrorMessage = "Пароль должен быть больше 8 символов")]
        public string Password { get; set; }
    }
}
