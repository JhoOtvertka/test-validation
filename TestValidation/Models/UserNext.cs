﻿using System.ComponentModel.DataAnnotations;

namespace TestValidation.Models
{
    public class UserNext
    {
        [Required]
        public string Login { get; set; }

        [RussianPassport]
        public string Passport { get; set; }
    }
}
